import React, { useEffect, useState } from "react";
import { FormProvider, useForm, useWatch } from "react-hook-form";
import { yupResolver } from "@hookform/resolvers/yup";
import { toast } from "react-hot-toast";

import { PrimaryButton } from "../../components/buttons/PrimaryButton";
import SelectInput from "../../components/inputs/SelectInput";

import tabs from "../../helpers/tabs";
import { getAvailableModules } from "../../helpers/modules/getAvailableModules";
import {
  setDefaultValuesByModule,
  resetFormValues,
  defaultFormValues,
} from "../../helpers/fields";

import { configureModule } from "../../services/configureModule";
import { getAvailableFieldsByModule } from "../../services/getAvailableFieldsByModule";

import { moduleValidator } from "../../validators/moduleValidator";

import {
  Container,
  FormWrapper,
  FormBody,
  SectionTitle,
  SectionDescription,
  ButtonsSection,
} from "./styles";

export default function AddModule({ changeTab, configuredModules }) {
  const [fieldOptions, setFieldOptions] = useState([]);
  const filteredModulesOptions = getAvailableModules(configuredModules.value);
  const formMethods = useForm({
    mode: "onChange",
    resolver: yupResolver(moduleValidator),
  });
  const module = useWatch({
    name: "module",
    control: formMethods.control,
    defaultValues: defaultFormValues,
  });

  function onCancel() {
    changeTab(tabs.listModules);
  }

  async function onSubmit(data) {
    const promise = configureModule(data);
    toast.promise(promise, {
      success: "Módulo configurado com sucesso!",
      error: "Erro ao configurar módulo. Tente novamente em alguns instantes.",
      loading: "Configurando módulo...",
    });

    promise
      .then(() => {
        configuredModules.forceRefresh();
        changeTab(tabs.listModules);
      })
      .catch((err) => console.log("Error configuring module: ", err));
  }

  useEffect(() => {
    if (module) {
      resetFormValues();
      const getModuleFields = async () => {
        const fieldsByModule = await getAvailableFieldsByModule(module.value);
        if (fieldsByModule) {
          setFieldOptions(fieldsByModule);
        }
      };

      getModuleFields();
    }
  }, [module]);

  useEffect(() => {
    if (fieldOptions?.length && module?.value) {
      setDefaultValuesByModule(formMethods, module.value, fieldOptions);
    }
  }, [fieldOptions]);

  return (
    <FormProvider {...formMethods}>
      <Container>
        <h1>Configurar Novo Módulo</h1>
        <FormWrapper>
          <SectionTitle>Módulo</SectionTitle>
          <SectionDescription>
            Aqui você deve configurar um módulo do seu Zoho CRM para que possa
            usufruir da automação.
          </SectionDescription>
          <FormBody onSubmit={formMethods.handleSubmit(onSubmit)}>
            <SelectInput
              options={filteredModulesOptions}
              label="Selecione um módulo"
              placeholder="Selecione"
              name="module"
            />

            <SectionTitle>Campos</SectionTitle>
            <SectionDescription>
              Após selecionar o módulo, você deve selecionar os campos do seu
              Zoho CRM que devem receber as informações de endereço.
            </SectionDescription>

            <SelectInput
              options={fieldOptions}
              label="Campo para o CEP"
              placeholder="Selecione"
              name="cep"
              disabled={!module && !fieldOptions.length}
              helpMessage="É a partir deste campo que todo o endereço será trazido."
            />

            <SelectInput
              options={fieldOptions}
              label="Campo para cidade"
              placeholder="Selecione"
              name="city"
              disabled={!module && !fieldOptions.length}
            />

            <SelectInput
              options={fieldOptions}
              label="Campo para estado"
              placeholder="Selecione"
              name="state"
              disabled={!module && !fieldOptions.length}
            />

            <SelectInput
              options={fieldOptions}
              label="Campo para rua"
              placeholder="Selecione"
              name="street"
              disabled={!module && !fieldOptions.length}
            />

            <SelectInput
              options={fieldOptions}
              label="Campo para bairro"
              placeholder="Selecione"
              name="neighbourhood"
              disabled={!module && !fieldOptions.length}
            />
            <ButtonsSection>
              <PrimaryButton
                onClick={onCancel}
                className="cancel-button"
                type="button"
              >
                Cancelar
              </PrimaryButton>
              <PrimaryButton type="submit">Salvar</PrimaryButton>
            </ButtonsSection>
          </FormBody>
        </FormWrapper>
      </Container>
    </FormProvider>
  );
}
