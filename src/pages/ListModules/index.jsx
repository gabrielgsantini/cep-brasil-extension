import React from "react";
import { toast } from "react-hot-toast";
import { PrimaryButton } from "../../components/buttons/PrimaryButton";
import { getFieldDisplayNameForConfiguredModule } from "../../helpers/fields/getFieldDisplayNameForConfiguredModule";
import tabs from "../../helpers/tabs";
import {
  Container,
  Header,
  ContentWrapper,
  SectionDescription,
  SectionTitle,
  TableContainer,
  TableHeader,
  TableHeaderItem,
  TableBody,
  TableRow,
  TableRowItem,
} from "./styles";

export default function ListModules({ changeTab, configuredModules }) {
  function handleAddModule() {
    if (configuredModules.value?.length === 3) {
      toast.error("Você pode configurar no máximo 3 módulos.");
      return;
    }

    changeTab(tabs.addModule);
  }

  return (
    <Container>
      <Header>
        <h1>Configurações</h1>
        <PrimaryButton onClick={handleAddModule} type="button">
          Adicionar Módulo
        </PrimaryButton>
      </Header>
      <ContentWrapper>
        <SectionTitle>Módulos configurados</SectionTitle>
        <SectionDescription>
          Aqui você tem a lista de todos os módulos configurados para que a CEP
          Brasil automatize sua busca de endereço.
        </SectionDescription>
        <TableContainer>
          <TableHeader>
            <TableHeaderItem>Módulo</TableHeaderItem>
            <TableHeaderItem>CEP</TableHeaderItem>
            <TableHeaderItem>Rua</TableHeaderItem>
            <TableHeaderItem>Bairro</TableHeaderItem>
            <TableHeaderItem>Cidade</TableHeaderItem>
            <TableHeaderItem>Estado</TableHeaderItem>
          </TableHeader>
          <TableBody>
            {!configuredModules?.value?.length ? (
              <TableRow>
                <TableRowItem notConfigured={true}>Nenhum módulo foi configurado até o momento.</TableRowItem>
              </TableRow>
            ) : (
              configuredModules?.value?.map((module, index) => {
                return (
                  <TableRow highlight={index % 2 !== 0}>
                    <TableRowItem>
                      {getFieldDisplayNameForConfiguredModule(module, "module")}
                    </TableRowItem>
                    <TableRowItem>
                      {getFieldDisplayNameForConfiguredModule(module, "cep")}
                    </TableRowItem>
                    <TableRowItem>
                      {getFieldDisplayNameForConfiguredModule(module, "street")}
                    </TableRowItem>
                    <TableRowItem>
                      {getFieldDisplayNameForConfiguredModule(
                        module,
                        "neighbourhood"
                      )}
                    </TableRowItem>
                    <TableRowItem>
                      {getFieldDisplayNameForConfiguredModule(module, "city")}
                    </TableRowItem>
                    <TableRowItem>
                      {getFieldDisplayNameForConfiguredModule(module, "state")}
                    </TableRowItem>
                  </TableRow>
                );
              })
            )}
          </TableBody>
        </TableContainer>
      </ContentWrapper>
    </Container>
  );
}
