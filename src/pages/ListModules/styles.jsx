import styled, { css } from "styled-components";

export const Container = styled.div`
  background-color: var(--background);
  color: var(--text);

  display: flex;
  flex-direction: column;

  min-width: 100vw;
  min-height: 100vh;
  max-width: 100vw;
  padding: 20px;
`;

export const Header = styled.div`
  display: flex;
  align-items: center;

  > button {
    margin-left: auto;
  }
`;

export const ContentWrapper = styled.div`
  margin-top: 20px;
  padding-bottom: 20px;

  border-radius: 30px;
  min-width: 100%;
  min-height: 100%;

  background: var(--white);

  > h2,
  p {
    padding-left: 30px;
    padding-right: 30px;
  }
`;

export const SectionTitle = styled.h2`
  margin-top: 20px;
`;

export const SectionDescription = styled.p`
  margin-bottom: 20px;
`;

export const TableContainer = styled.div`
  width: 100%;
`;
export const TableHeader = styled.div`
  display: flex;
  width: 100%;
  padding: 10px;
  font-weight: 700;
  padding-left: 30px;
  padding-right: 30px;
`;

export const TableHeaderItem = styled.div`
  flex: 1 1 20%;
  text-align: left;
  font-size: 14px;
`;
export const TableBody = styled.div``;

export const TableRow = styled.div`
  display: flex;
  position: relative;

  width: 100%;

  padding: 10px;
  padding-top: 20px;
  padding-bottom: 20px;
  padding-left: 30px;
  padding-right: 30px;

  cursor: pointer;

  background: ${(props) =>
    props.highlight ? `var(--background)` : `var(--white)`};

  transition: 180ms all;

  &:hover {
    filter: brightness(0.99);
  }

  &:nth-of-type(odd) {
    background: $color-form-highlight;
  }
`;

export const TableRowItem = styled.div`
  flex: 1 1 20%;
  text-align: left;
  white-space: nowrap;
  overflow: hidden;
  text-overflow: ellipsis;
  padding-right: 10px;
  font-weight: 500;

  ${(props) =>
    props.notConfigured &&
    `
    ${notConfiguredCSS}
  `}
`;

const notConfiguredCSS = css`
  font-style: italic;
  font-weight: 700;
  color: var(--gray-500);
`;
