import styled from "styled-components";

export const Container = styled.div`
  background-color: var(--background);
  color: var(--text);

  display: flex;
  flex-direction: column;

  min-width: 100vw;
  min-height: 100vh;
  max-width: 100vw;
  padding: 20px;
`;

export const FormWrapper = styled.div`
  margin-top: 20px;
  padding-top: 14px;
  padding-bottom: 14px;

  padding-left: 30px;
  padding-right: 30px;

  border-radius: 30px;
  min-width: 100%;
  min-height: 100%;

  background: var(--white);
`;

export const FormBody = styled.form`
  min-width: 100%;
  min-height: 100%;
  display: flex;
  flex-direction: column;

  margin-top: 20px;

  > .field-container + .field-container {
    margin-top: 15px;
  }
`;

export const SectionTitle = styled.h2`
  margin-top: 20px;
`;

export const SectionDescription = styled.p`
  margin-bottom: 20px;
`;

export const ButtonsSection = styled.div`
  display: flex;
  min-width: 100%;
  justify-content: flex-end;

  > .cancel-button {
    background: var(--background);
    color: var(--text);
    border: 1px solid var(--text);
  }

  > button {
    margin-top: 20px;
  }

  > button + button {
    margin-left: 10px;
  }
`;
