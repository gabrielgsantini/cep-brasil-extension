import { useEffect, useState } from "react";
import { Toaster } from "react-hot-toast";
import AddModule from "./pages/AddModule";
import ListModules from "./pages/ListModules";
import EditModule from "./pages/EditModule";

import tabs from "./helpers/tabs";
import { useTrackFunction } from "./hooks/useTrackFunction";
import { getConfiguredModules } from "./services/getConfiguredModules";

import "./App.css";
import GlobalStyles from "./styles/GlobalStyles";

function App() {
  const configuredModules = useTrackFunction(getConfiguredModules);
  const [currentTab, setCurrentTab] = useState(tabs.listModules);
  const [params, setParams] = useState({});

  function changeParams(newParams) {
    setParams((prevParams) => ({ ...prevParams, ...newParams }));
  }

  function resetParams() {
    setParams({});
  }

  return (
    <div>
      {currentTab === tabs.addModule && (
        <AddModule
          configuredModules={configuredModules}
          changeTab={setCurrentTab}
        />
      )}

      {currentTab === tabs.editModule && (
        <EditModule
          configuredModules={configuredModules}
          changeTab={setCurrentTab}
          params={params}
          resetParams={resetParams}
        />
      )}

      {currentTab === tabs.listModules && (
        <ListModules
          changeParams={changeParams}
          changeTab={setCurrentTab}
          configuredModules={configuredModules}
        />
      )}

      <GlobalStyles />
      <Toaster
        toastOptions={{
          style: {
            color: "var(--primary-blue)",
            font: '700 14px "Inter"',
            borderRadius: "0px",
          },
          iconTheme: {
            primary: "var(--secondary-blue)",
            secondary: "var(--white)",
          },
          duration: 1000,
        }}
      />
    </div>
  );
}

export default App;
