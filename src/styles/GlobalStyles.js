import { createGlobalStyle } from "styled-components";
export default createGlobalStyle`
* {
    margin: 0;
    padding: 0;
    box-sizing: border-box;

    font-family: 'Inter', sans-serif;

    ::-webkit-scrollbar {
        display: none;
    }

    @media (min-width: 732px) {
        ::-webkit-scrollbar {
            display: flex;
        }
    }

    
}

body {
    background:  #FAF9F9;
    color: #000000;
    
    ::-webkit-scrollbar {
        display: none;
    }
}

:root {
        --background: #FAF9F9;
        --text: #11263C;
        --secondary-text: rgba(136, 136, 157, 1);
        --white: #FFFFFF;
        --semi-black: #0E0E0E;
        --black: #000000;
        --white-gray: #FAF9F9;
        --placeholder: #8e8e8e;

        --gray-100: #F5F5F5;
        --gray-150: #898F97;
        --gray-200: #8E8EA1;
        --gray-500: #686868;
        --gray-700: #2F2F31;
        --gray-750: #1A1A1C;
        --gray-800: #151515;
        --gray-900: #0B0B0C;
        --border-gray: #686868;
        --input-gray: rgba(21, 21, 21, 1);

        --error: #ee4035;
        
        --primary-blue: #4274e3;
        --secondary-blue:#32a1ee;
        
    }

body, input, button, textarea, p, a, span {
    font: 400 12px 'Inter', sans-serif;
    @media (min-width: 1005px) {
    font: 400 16px 'Inter', sans-serif;
    }
}

h1, strong {
    font: 700 24px 'Inter', sans-serif;
}

h2 {
    font: 700 16px 'Inter', sans-serif;
}

p {
    font: 400 12px 'Inter', sans-serif;
    color: var(--secondary-text);
    margin-top: 5px;
    margin-bottom: 5px;
}

span {
    font: 400 12px 'Inter', sans-serif;
    color: var(--secondary-text);
}

input, textarea {
    color: var(--white) !important;
  ::placeholder {
    color: var(--border-gray) !important;
  }
}

label {
    font: 500 12px 'Inter', sans-serif;
    color: var(--text);
}
`;
