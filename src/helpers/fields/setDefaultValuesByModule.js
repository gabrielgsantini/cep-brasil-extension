import { getDefaultFieldsByModule } from "./defaultFieldsByModule";

export function setDefaultValuesByModule(
  formMethods,
  module = "",
  availableFields = []
) {
  const defaultFieldValuesByModule = getDefaultFieldsByModule(module);

  defaultFieldValuesByModule.forEach((field) => {
    const fieldValues = availableFields.find((f) => f.value === field.value);
    if (fieldValues) {
      formMethods?.setValue(field.key, fieldValues);
    }
  });
}
