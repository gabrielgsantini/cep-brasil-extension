const defaultMessage = "Não configurado";

export function getFieldDisplayNameForConfiguredModule(
  configuredModule,
  field
) {
  if (!configuredModule || !field) {
    return defaultMessage;
  }

  switch (field) {
    case "module":
      return configuredModule.Name || defaultMessage;
    case "cep":
      return (
        configuredModule.zohocrmtestinggabrielsantini__Exibicao_campo_para_CEP ||
        defaultMessage
      );
    case "street":
      return (
        configuredModule.zohocrmtestinggabrielsantini__Exibicao_campo_para_rua ||
        defaultMessage
      );
    case "neighbourhood":
      return (
        configuredModule.zohocrmtestinggabrielsantini__Exibicao_campo_para_bairro ||
        defaultMessage
      );
    case "city":
      return (
        configuredModule.zohocrmtestinggabrielsantini__Exibicao_campo_para_cidade ||
        defaultMessage
      );
    case "state":
      return (
        configuredModule.zohocrmtestinggabrielsantini__Exibicao_campo_para_estado ||
        defaultMessage
      );
    default:
      return defaultMessage;
  }
}
