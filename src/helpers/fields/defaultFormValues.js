export const defaultFormValues = {
  cep: undefined,
  city: undefined,
  state: undefined,
  street: undefined,
  neighbourhood: undefined,
};
