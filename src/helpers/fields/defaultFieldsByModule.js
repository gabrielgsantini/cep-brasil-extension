const defaultFieldsByModule = {
  Accounts: {
    cep: "Billing_Code",
    street: "Billing_Street",
    neighbourhood: "zohocrmtestinggabrielsantini__Bairro",
    city: "Billing_City",
    state: "Billing_State",
  },
  Contacts: {
    cep: "Mailing_Zip",
    street: "Mailing_Street",
    neighbourhood: "zohocrmtestinggabrielsantini__Bairro",
    city: "Mailing_City",
    state: "Mailing_State",
  },
  Leads: {
    cep: "Zip_Code",
    street: "Street",
    neighbourhood: "zohocrmtestinggabrielsantini__Bairro",
    city: "City",
    state: "State",
  },
};

export function getDefaultFieldsByModule(module) {
  const fieldsByModule = defaultFieldsByModule?.[module];
  if (!fieldsByModule) {
    return [];
  }

  return Object.entries(fieldsByModule).map(([key, value]) => {
    return { key, value };
  });
}
