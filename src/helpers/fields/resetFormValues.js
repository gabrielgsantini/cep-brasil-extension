import { defaultFormValues } from "./defaultFormValues";

export function resetFormValues(formMethods) {
  Object.keys(defaultFormValues).forEach((field) => {
    formMethods.resetField(field);
  });
}
