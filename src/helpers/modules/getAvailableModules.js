import { modules } from "./modulesOptions";

export function getAvailableModules(configuredModules) {
  const availableModules = modules.filter((option) => {
    const invalid = configuredModules?.some(
      (configured) => configured.Name === option?.value
    );
    return !invalid;
  });

  return availableModules;
}
