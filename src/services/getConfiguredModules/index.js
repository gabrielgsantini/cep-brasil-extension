import { ZOHO } from "../../vendor/ZSDK";

export async function getConfiguredModules() {
  await ZOHO.embeddedApp.init();

  const getAllModulesResult = await ZOHO.CRM.API.getAllRecords({
    Entity: "zohocrmtestinggabrielsantini__CEP_Brasil_Settings",
  });
  console.log("Get all configured modules result: ", getAllModulesResult);
  const configuredModules = getAllModulesResult.data;
  return configuredModules;
}
