import { ZOHO } from "../../vendor/ZSDK";

const defaultData = {
  cep: {
    label: "",
    value: "",
  },
  state: {
    label: "",
    value: "",
  },
  street: {
    label: "",
    value: "",
  },
  city: {
    label: "",
    value: "",
  },
  neighbourhood: {
    label: "",
    value: "",
  },
  module: {
    label: "",
    value: "",
  },
};

export async function configureModule(data = defaultData) {
  const formattedApiData = formatApiData(data);
  const configureResult = await ZOHO.CRM.API.insertRecord({
    Entity: "zohocrmtestinggabrielsantini__CEP_Brasil_Settings",
    APIData: formattedApiData,
  });
  console.log("Configure Result");

  return configureResult?.data;
}

function formatApiData(data = defaultData) {
  return [
    {
      Name: data.module.value,
      zohocrmtestinggabrielsantini__Campo_para_CEP: data.cep.value,
      zohocrmtestinggabrielsantini__Campo_para_estado: data.state.value,
      zohocrmtestinggabrielsantini__Campo_para_rua: data.street.value,
      zohocrmtestinggabrielsantini__Campo_para_cidade: data.city.value,
      zohocrmtestinggabrielsantini__Campo_para_bairro: data.neighbourhood.value,
      
      zohocrmtestinggabrielsantini__Exibicao_campo_para_bairro:
        data.neighbourhood.label,
      zohocrmtestinggabrielsantini__Exibicao_campo_para_CEP: data.cep.label,
      zohocrmtestinggabrielsantini__Exibicao_campo_para_cidade: data.city.label,
      zohocrmtestinggabrielsantini__Exibicao_campo_para_estado:
        data.state.label,
      zohocrmtestinggabrielsantini__Exibicao_campo_para_rua: data.street.label,
    },
  ];
}
