import { ZOHO } from "../../vendor/ZSDK";

export async function getAvailableFieldsByModule(module = "Leads") {
  try {
    await ZOHO.embeddedApp.init();
    const fields = await ZOHO.CRM.META.getFields({ Entity: module });
    console.log("FieldsResult", fields);
    const filtered = filterFieldsByType(fields.fields);
    const formatted = formatFields(filtered || []);
    return formatted || [];
  } catch (err) {
    console.log("error getting available fields by module: ", err);
  }
}

function formatFields(fields = []) {
  const formatted = fields.map((field) => {
    return {
      label: `${field.field_label}`,
      value: field.api_name,
    };
  });

  return formatted;
}

function filterFieldsByType(fields = []) {
  const validTypes = ["text", "picklist"];
  const validFields = fields.filter((field) => {
    return validTypes.includes(field.data_type);
  });
  return validFields;
}
