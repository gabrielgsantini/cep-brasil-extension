import { useEffect, useState } from "react";

export function useTrackFunction(functionToBeCalled, params) {
  const [isLoading, setIsLoading] = useState(true);
  const [value, setValue] = useState(null);
  const [error, setError] = useState(null);

  useEffect(() => {
    functionToBeCalled(params)
      .then((response) => {
        setValue(response);
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }, [functionToBeCalled, params]);

  function forceRefresh() {
    setIsLoading(true);

    functionToBeCalled(params)
      .then((response) => {
        setValue(response);
      })
      .catch((error) => {
        setError(error);
      })
      .finally(() => {
        setIsLoading(false);
      });
  }

  return { isLoading, value, error, forceRefresh };
}
