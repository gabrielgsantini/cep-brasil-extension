import * as yup from "yup";

const invalidMessage = (fieldName) =>
  `Selecione um campo válido para ${fieldName}.`;

const selectInputSchema = (errorMessage) => {
  return yup.object().shape({
    label: yup.string(errorMessage).required(errorMessage),
  });
};

export const moduleValidator = yup.object().shape({
  module: selectInputSchema("Selecione um Módulo válido."),
  cep: selectInputSchema(invalidMessage("CEP")),
  city: selectInputSchema(invalidMessage("Cidade")),
  state: selectInputSchema(invalidMessage("Estado")),
  street: selectInputSchema(invalidMessage("Rua")),
  neighbourhood: selectInputSchema(invalidMessage("Bairro")),
});
