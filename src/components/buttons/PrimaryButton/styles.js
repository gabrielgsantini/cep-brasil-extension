import styled from "styled-components";

export const Container = styled.button`
  width: 200px;
  height: 45px;

  background: var(--primary-blue);
  color: var(--white);

  border-radius: 30px;

  display: flex;
  align-items: center;
  justify-content: center;

  outline: 0;
  border: none;

  transition: 180ms all;
  font-weight: 700;
  cursor: pointer;
  
  &:hover {
    filter: brightness(0.92);
  }
`;
