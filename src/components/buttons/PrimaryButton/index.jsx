import React from "react";
import { Container } from "./styles";

export function PrimaryButton({ children, ...props }) {
  return <Container {...props}>{children}</Container>;
}
