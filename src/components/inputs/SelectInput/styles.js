import styled from "styled-components";

export const SelectInputContainer = styled.div`
  > .select-base-input {
    margin-top: 10px;
    > .select__control {
      border-color: var(--text);
      > .select__value-container {
        > .select__placeholder {
          font-family: "Inter", sans-serif !important;
          font-weight: 500;
        }

        > .select__single-value {
          font-family: "Inter", sans-serif !important;
          font-weight: 500;
          color: var(--text);
        }

        > .select__input-container {
          > input {
            font-family: "Inter", sans-serif !important;
            font-weight: 500 !important;
            color: var(--text) !important;
          }
        }
      }
    }

    > .select__control--is-focused {
      border-color: var(--text);
      box-shadow: none;
    }

    > .select__menu {
      > .select__menu-list {
        > .select__option {
          font-weight: 500;
        }
      }
    }
  }
`;

export const HelpMessage = styled.span`
  margin-top: 5px;
  display: flex;
`;

export const ErrorMessage = styled.span`
  margin-top: 5px;
  display: flex;
  color: var(--error);
`;
