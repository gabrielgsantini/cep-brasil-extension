import React from "react";
import ReactSelect from "react-select";
import { Controller, useFormContext, useWatch } from "react-hook-form";

import { ErrorMessage, HelpMessage, SelectInputContainer } from "./styles";

const selectInputDefaultProps = {
  options: [{ label: "", value: "" }],
  label: "",
  value: "",
  onChange: (value = { label: "", value: "" }) => {},
  placeholder: "",
  name: "",
  disabled: false,
  helpMessage: "",
};

export default function SelectInput({
  label,
  ...props
} = selectInputDefaultProps) {
  const {
    control,
    formState: { errors },
  } = useFormContext();
  const fieldValues = useWatch({ control, name: props.name });
  return (
    <SelectInputContainer className="field-container">
      <label> {label} </label>

      <Controller
        name={props.name}
        control={control}
        render={({ field }) => (
          <ReactSelect
            {...field}
            name={props.name}
            placeholder={props.placeholder}
            options={props.options}
            isDisabled={props.disabled}
            noOptionsMessage={() => "Nenhuma opção disponível"}
            className="select-base-input"
            classNamePrefix="select"
          />
        )}
      />
      {errors?.[props.name] && (
        <ErrorMessage>{errors?.[props.name]?.label?.message}</ErrorMessage>
      )}
      {props.helpMessage && <HelpMessage>{props.helpMessage}</HelpMessage>}
      {fieldValues?.value?.data_type === "picklist" && (
        <HelpMessage>
          O campo selecionado é do tipo "Lista de opções", certifique-se de que
          as opções do campo estão de acordo ao esperado.
        </HelpMessage>
      )}
    </SelectInputContainer>
  );
}
